#!/bin/bash
id=$1

trap ctrl_c INT
function ctrl_c() {
        rm $id.pipe
        exit 0
}

if [ $# -eq 1 ]; then
        mkfifo $id.pipe
        while true;do
                echo pls input
                read cmd db table col
                echo $id $cmd $db $table $col> server.pipe
                read word < $id.pipe
                ss=`echo $word | grep -c start`
                echo $word              
                while [ $ss -eq 1 ];do
                        read data < $id.pipe
                        echo $data
                        ss=`echo $data | grep -c end`
                        ss=$((ss+1))
                done
                sd=`echo $word | grep -c shutdown`
                if [ $sd -gt 0 ];then
                        rm $id.pipe
                        exit 0
                fi
                sleep 1
        done
else
        echo "parameters problem"
fi
