#!/bin/bash
if [ $# -ne 4 ]; then
        echo "parameters problem" > $1.pipe
        echo "command executes failed"
        exit 1
elif[ ! -d $2 ];
        echo "DB does not exist" > $1.pipe
        echo "command executes failed"
        exit 2
elif[ ! -e $2/$3 ];
        echo "table does not exist" > $1.pipe
        echo "command executes failed"
        exit 3
else
        echo $4 > testing.txt
        number=`head -n1 $2/$3 | grep -o "," | wc -l`
        numberl=`head -n1 testing.txt | grep -o "," | wc -l`

        if [ $number -eq $numberl ]; then
                echo $4 >> $2/$3
                echo "tuple inserted" > $1.pipe
                echo "command executes successfully"
                exit 0
        else
                echo "number of columns in tuple does not match schema" > $4.pipe
                echo "command executes failed"
                exit 4
        fi
        rm testing.txt

fi