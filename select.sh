#!/bin/bash


if [ $# -ne 4 ];then
        echo "parameters problem" > $1.pipe
        echo "command executes failed"
        exit 1
elif [  ! -d $2 ];then
        echo "DB does not exist" > $1.pipe
        echo "command executes failed"
        exit 2
elif [ ! -e $2/$3 ];then
        echo "table does not exist" > $1.pipe
        echo "command executes failed"
        exit 3
else
        num=`head -n1 $2/$3 | grep -o "," | wc -l`
        num=$((num+1))
        for (( i = 1; i <= $num; i++ ));do
                echo "$i"
        done > s
        echo $4 | sed 's/,/\n/g' > sl
        vn=`egrep -f s -v sl | wc -c`
        if [ $vn -eq 0 ];then
                echo start > $1.pipe
                cat $2/$3 | cut -d , -f $4 > TEMP
                while read line;do
                        echo $line > $1.pipe
                        sleep 0.1
                done < TEMP
                echo end > $1.pipe
                echo "command executes successfully"
        else
                echo "column does not exist" > $1.pipe
                echo "command executes failed"
                exit 4
        fi
        rm s sl TEMP
        exit 0
fi