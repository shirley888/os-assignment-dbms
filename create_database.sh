#!/bin/bash

if [ $# -ne 2 ]; then
        echo "Error: no parameter" > $1.pipe
        echo "command executes failed"
        exit 1
else
        if [ -d $2 ]; then
                echo "DB already exists" > $1.pipe
                echo "command executes failed"
                exit 2
        else
                mkdir $2
                echo "database created" > $1.pipe
                echo "command executes successfully"
                exit 0
        fi

fi