#!/bin/bash

if [ $# -ne 4 ]; then
        echo "parameters problem" > $1.pipe
        echo "command executes failed"
        exit 1
elif[ ! -d $2 ];
        echo "DB does not exist" > $1.pipe
        echo "command executes failed"
        exit 2
else
        if [ -e $2/$3 ]; then
                echo "table already exists" > $1.pipe
                echo "command executes failed"
                exit 3
        else
                touch $1/$2
                echo "$4" >> $2/$3
                echo "table created" > $1.pipe
                echo "command executes successfully"
                exit 0

        fi
fi
