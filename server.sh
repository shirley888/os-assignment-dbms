#!/bin/bash

mkfifo server.pipe

trap ctrl_c INT
function ctrl_c() {
        rm server.pipe
        exit 0
}

while true; do
        sleep 2
        read id cmd db table col < server.pipe

        if [ $cmd == "shutdown" ];then
                echo "shutdown system" > $id.pipe
                rm server.pipe
                exit 0
        else

        while ! mkdir $db.lock ;do
                sleep1
        done

case "$cmd" in
        create_database)
                ./create_database.sh $id $db;;
        create_table)
                ./create_table.sh $id $db $table $col;;
        insert)
                ./insert.sh $id $db $table $col;;
        select)
                ./select.sh $id $db $table $col;;
        *)
                echo "Error: bad request"
                exit 1;;
esac
fi
rm -d $db.lock
done